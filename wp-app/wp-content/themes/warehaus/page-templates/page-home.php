<?php
/**
 * Template Name: Home Page
 *
 * @package Warehaus
 * @subpackage warehaus
 * @since warehaus 1.0
 */

get_header();
?>
                                                                            src="<?php echo get_template_directory_uri() ?>/assets/images/Line2x.png"
                                                                            width="144">
            <p class="paragraph paragraphoverlay"><?php echo get_field( 'hero_subtext' ) ?></p>
            <a class="button w-button" target="_blank"
               href="<?php echo get_field( 'cta_button_link' ) ?>"><?php echo get_field( 'cta_button_text' ) ?></a>
        </div>
    </div>
    <div class="section-2 slant-bottom">
        <div class="container-4 w-container">
            <div>
                <h2 class="heading"><?php echo get_field( 'banner_title_1' ) ?></h2>

                <img class="image-3" sizes="(max-width: 479px) 100vw, 700px"
                     src="<?php echo get_template_directory_uri() ?>/assets/images/Title-Graphic-Background.png"
                     srcset="<?php echo get_template_directory_uri() ?>/assets/images/Title-Graphic-Background-p-500.png 500w, <?php echo get_template_directory_uri() ?>/assets/images/Title-Graphic-Background.png 784w">
            </div>
            <h1 class="center dark h1"><?php echo get_field( 'banner_title_2' ) ?></h1>
            <div class="text-block"><?php echo get_field( 'banner_subtext' ) ?></div>
        </div>
    </div>
    <div>
        <div class="container-6 w-container"><img class="image-4"
                                                  sizes="(max-width: 767px) 80vw, (max-width: 991px) 582.390625px, 752px"
                                                  src="<?php echo get_template_directory_uri() ?>/assets/images/Section-3.png"
                                                  srcset="<?php echo get_template_directory_uri() ?>/assets/images/Section-3-p-500.png 500w, <?php echo get_template_directory_uri() ?>/assets/images/Section-3-p-800.png 800w, <?php echo get_template_directory_uri() ?>/assets/images/Section-3-p-1080.png 2000w, <?php echo get_template_directory_uri() ?>/assets/images/Section-3.png 1500w">
        </div>
    </div>
    <div class="section-5 slant-top"
         style="background-image: url(<?php echo get_field( 'hero_2_image' ) ?>);">
        <div class="w-container">
            <h2 class="heading-4"><?php echo get_field( 'hero_2_title_1' ) ?></h2>
            <h1 class="dark h1 right"><?php echo get_field( 'hero_2_title_2' ) ?></h1>
            <p class="paragraph-2"><?php echo get_field( 'hero_2_subtext' ) ?></p>
            <a class="button-2 w-button" target="_blank"
               href="<?php echo get_field( 'cta_2_button_link' ) ?>"><?php echo get_field( 'cta_2_button_text' ) ?></a>
        </div>
    </div>
    <div class="section-7">
        <h2 class="h2 orange"><?php echo get_field( 'banner_2_title_1' ) ?></h2>
        <h1 class="center dark h1"><?php echo get_field( 'banner_2_title_2' ) ?></h1>
    </div>
    <div class="sliderpage">
        <div class="slider-2 w-slider" data-animation="slide" data-duration="500" data-infinite="1">
            <div class="w-slider-mask">
				<?php
				if ( have_rows( 'testimonials' ) ):
					while ( have_rows( 'testimonials' ) ) : the_row();
						?>
                        <div class="<?php echo get_field('slide_class') ?> w-slide">
                            <div class="slide-testimonail">
                                <div class="div-block-4">
                                    <p class="sliderquote"><?php the_sub_field( 'quote' ); ?></p>
                                    <h3 class="h2 quote-name"><?php the_sub_field( 'name' ) ?></h3>
                                    <h4 class="h3 quote-sub-title"><?php the_sub_field( 'subtitle' ) ?></h4>
                                </div>
                                <img class="image-12" sizes="40vw" src="<?php the_sub_field( 'image' ) ?>">
                            </div>
                        </div>
						<?php
					endwhile;
				endif;
				?>
            </div>
            <div class="w-slider-arrow-left">
                <div class="sliderarrow w-icon-slider-left"></div>
            </div>
            <div class="w-slider-arrow-right">
                <div class="sliderarrow w-icon-slider-right"></div>
            </div>
            <div class="w-round w-slider-nav"></div>
        </div>
    </div>

<?php
get_footer();